package com.groupproject.votingsystem.domain.region;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/region")
public class RegionController {

    private RegionRepository regionRepository;

    private RegionController(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @GetMapping("name/{name}")
    List<Region> getAllRegionsByName(@PathVariable String name) {
        return regionRepository.findAllByVoivodeshipContainingIgnoreCaseOrCountContainingIgnoreCaseOrCommuneContainingIgnoreCase(name, name, name);
    }

    @GetMapping("id/{id}")
    public Region getRegionById(@PathVariable Long id) {
        return regionRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Region> getAll() {
        return regionRepository.findAll();
    }
}
