package com.groupproject.votingsystem.domain.region;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface RegionRepository extends JpaRepository<Region, Long>, JpaSpecificationExecutor {

    List<Region> findAllByVoivodeshipContainingIgnoreCaseOrCountContainingIgnoreCaseOrCommuneContainingIgnoreCase(String voivodeship, String count, String commune);
}
