package com.groupproject.votingsystem.domain.candidate;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/candidate")
public class CandidateController {

    private final CandidateRepository candidateRepository;

    private CandidateController(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @GetMapping("voteType/name/{voteTypeName}")
    List<Candidate> getAllByVoteTypeName(@PathVariable String voteTypeName) {
        return candidateRepository.findAllByVoteType_NameContainingIgnoreCase(voteTypeName);
    }

    @GetMapping("voteType/region/{voteTypeRegion}")
    List<Candidate> getAllByVoteTypeRegion(@PathVariable String voteTypeRegion) {
        return candidateRepository.findAllByVoteType_Region_VoivodeshipContainingIgnoreCaseOrVoteType_RegionCountContainingIgnoreCaseOrVoteType_Region_CommuneContainingIgnoreCase(voteTypeRegion, voteTypeRegion, voteTypeRegion);
    }

    @GetMapping("person/name/{personName}")
    List<Candidate> getAllByPerson(@PathVariable String personName) {
        return candidateRepository.findAllByPerson_FirstNameContainingIgnoreCaseOrPerson_LastNameContainingIgnoreCase(personName, personName);
    }

    @GetMapping("person/pesel/{personPesel}")
    List<Candidate> getAllByPersonPesel(@PathVariable Long personPesel) {
        return candidateRepository.findAllByPerson_PeselEquals(personPesel);
    }

    @GetMapping("person/idCardNumber/{personIdCardNumber}")
    List<Candidate> getAllByPersonIdCardNumber(@PathVariable String personIdCardNumber) {
        return candidateRepository.findAllByPerson_IdCardNumberEqualsIgnoreCase(personIdCardNumber);
    }

    @GetMapping("person/region/{personRegion}")
    List<Candidate> getAllByPersonRegion(@PathVariable String personRegion) {
        return candidateRepository.findAllByPerson_Region_VoivodeshipContainingIgnoreCaseOrPerson_RegionCountContainingIgnoreCaseOrPerson_Region_CommuneContainingIgnoreCase(personRegion, personRegion, personRegion);
    }

    @GetMapping("person/city/{personCity}")
    List<Candidate> getAllByPersonCity(@PathVariable String personCity) {
        return candidateRepository.findAllByPerson_CityContainingIgnoreCase(personCity);
    }

    @GetMapping("party/{party}")
    List<Candidate> getAllByParty(@PathVariable String party) {
        return candidateRepository.findAllByParty_NameContainingIgnoreCase(party);
    }

    @GetMapping("listPosition/{listPosition}")
    List<Candidate> getAllByListPosition(@PathVariable Long listPosition) {
        return candidateRepository.findAllByListPositionEquals(listPosition);
    }

    @GetMapping("id/{id}")
    public Candidate getById(@PathVariable Long id) {
        return candidateRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Candidate> getAll() {
        return candidateRepository.findAll();
    }

    @PostMapping
    public Candidate add(@Valid @RequestBody Candidate candidate) {
        System.out.println(candidate);
        return candidateRepository.saveAndFlush(candidate);
    }

    @PutMapping("{id}")
    public ResponseEntity edit(@PathVariable Long id, @Valid @RequestBody Candidate candidate) {
        if (!id.equals(candidate.getId())) {
            return ResponseEntity.badRequest().build();
        }
        Optional<Candidate> existingCandidate = candidateRepository.findById(id);
        if (existingCandidate.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        candidateRepository.save(candidate);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        Optional<Candidate> existingCandidate = candidateRepository.findById(id);
        if (existingCandidate.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        candidateRepository.deleteById(id);

        return ResponseEntity.ok().build();
    }
}
