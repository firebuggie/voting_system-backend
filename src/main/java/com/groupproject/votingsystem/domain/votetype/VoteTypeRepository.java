package com.groupproject.votingsystem.domain.votetype;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VoteTypeRepository extends JpaRepository<VoteType, Long> {

    List<VoteType> findAllByNameContainingIgnoreCase(String name);

    List<VoteType> findAllByRegion_VoivodeshipContainingIgnoreCaseOrRegionCountContainingIgnoreCaseOrRegion_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);
}
