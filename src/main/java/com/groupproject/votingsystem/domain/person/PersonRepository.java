package com.groupproject.votingsystem.domain.person;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {

    List<Person> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String firstName, String lastName);

    List<Person> findAllByPeselEquals(Long pesel);

    List<Person> findAllByIdCardNumberEqualsIgnoreCase(String idCardNumber);

    List<Person> findAllByRegion_VoivodeshipContainingIgnoreCaseOrRegionCountContainingIgnoreCaseOrRegion_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);

    List<Person> findAllByCityContainingIgnoreCase(String city);
}
