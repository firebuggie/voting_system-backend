package com.groupproject.votingsystem.domain.party;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(schema = "PUBLIC", name = "PARTY")
public class Party {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @NotBlank
    @Length(max = 30)
    @Column(name = "NAME")
    private String name;

    public Party() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
